# Lab5 -- Integration testing


## Specs

- Budet car price per minute = 26
- Luxury car price per minute = 65
- Fixed price per km = 26
- Allowed deviations in % = 12
- Inno discount in % = 8

## Inputs

- type: budget, luxury, nonsense,
- plan: minute, fixed_price, nonsense,
- distance: -10, 0, 10, 100000, nonsense,
- planned_distance: -10, 0, 10, 1000000, nonsense,
- time: -10, 0, 10, 100000, nonsense,
- planed_time: -10, 0, 10, 100000, nonsense,
- inno_discount: yes, no, nonsense

## BVA and Decisions tables
![bva](tables/bva.png)
![decisions](tables/decisions.png)

## Bugs

* type "nonsense" returns random values (153.33), not invalid request
* negative value for planed distance (-10) returns calculated values, not Invalid request. Calculations work based on distance without using planed distance
* fixed_price with time return random value as calculations, even in fixed_time plan
* nonsense value for time and minute plan returns "null" not Invalid request
* planed_time doesn't affecct on calculations in minute plan, even in case of nonsense value
* nonsense value for distance (or planed distance) in fixed_price returns predefined value not Invalid request
* nonsence value for time in fixed_price plan returns predefined value
* difference in time and planed_time return predefined value
* border values (in case of test 100000) for distance, planned_distance, time, planed_time return predefined value
